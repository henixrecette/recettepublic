# Priorité d'automatisation: null
# Importance du cas de test: Faible
# language: fr
Fonctionnalité: BDD

	Plan du scénario: BDD
		Étant donné que Je marche
		Quand Je décide de courrir
		Alors Je vais plus vite
		Et je sélectionne le <habit>.

		@Chausette
		Exemples:
		| habit |
		| "Chausette" |

		@Paréo
		Exemples:
		| habit |
		| "Paréo" |